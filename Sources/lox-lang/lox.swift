import Foundation

struct Lox {
    var hadError = false;

    func runFile() {
        guard let file = readFile(path: CommandLine.arguments[1]) else {
            print("Could not read lox file")
            exit(1)
        }

        run(file)

        if (hadError) { exit(1) }
    }

    func runPrompt() {
        while true {
            print("> ", terminator: "")
            if let line = readLine() {
                run(line)
                hadError = false
            } else {
                print("Could not read input")
                exit(1)
            }
        }
    }

    func run(source: String) {
        let scanner = Scanner(source: source)
        let tokens = scanner.scanTokens()

        for token in tokens {
            print(token)
        }
    }

    func error(line: Int, message: String) {
        report(line: line, place: "", message: message)
    }

    func report(line: Int, place: String, message: String) {
        print("[Line \(line)] Error\(place): \(message)")
        hadError = true
    }
}