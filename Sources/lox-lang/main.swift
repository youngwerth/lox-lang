import Foundation

let lox = Lox()

switch CommandLine.arguments.count {
case 1:
    lox.runPrompt();
case 2:
    lox.runFile(CommandLine.arguments[1])
default:
    print("Usage: lox <path-to-src>")
    exit(1)
}