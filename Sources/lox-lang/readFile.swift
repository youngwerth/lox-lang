import Foundation

func readFile(path: String) -> String? {
    let url = URL(fileURLWithPath: path)
    return try? String(contentsOf: url, encoding: .utf8)
}