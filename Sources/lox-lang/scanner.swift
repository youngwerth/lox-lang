import Foundation

class Scanner {
    let source: String
    let tokens: [Token] = [Token]()
    private start = 0
    private current = 0
    private line = 1

    func scanTokens() -> [Token] {
        while !isAtEnd() {
            start = current
            scanToken();
        }

        tokens.append(Token(token: .EOF, lexeme: "", literal: .NIL, line))
        return tokens
    }

    private func isAtEnd() -> Bool {
        return current >= source.count
    }

    private func scanToken() {
        let c = advance();

        switch c {
        case "(":
            addToken(.LEFT_PAREN)
        case ")":
            addToken(.RIGHT_PAREN)
        case "{":
            addToken(.LEFT_BRACE)
        case "}":
            addToken(.RIGHT_BRACE)
        case ",":
            addToken(.COMMA)
        case ".":
            addToken(.DOT)
        case "-":
            addToken(.MINUS)
        case "+":
            addToken(.PLUS)
        case ";":
            addToken(.SEMICOLON)
        case "*":
            addToken(.STAR)
        case "!":
            addToken(match("=") ? TokenType.BANG_EQUAL : TokenType.BANG)
        case "=":
            addToken(match("=") ? TokenType.EQUAL_EQUAL : TokenType.EQUAL)
        case "<":
            addToken(match("=") ? TokenType.LESS_EQUAL : TokenType.LESS)
        case ">":
            addToken(match("=") ? TokenType.GREATER_EQUAL : TokenType.GREATER)
        case "/":
            if (match('/')) {
                while (peek() != "\n") && !isAtEnd() { advance() };
            } else {
                addToken(.SLASH);
            }
        case " ", "\r", "\t":
            break
        case "\n":
            line += 1
        case "\"":
            string()
        case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
            number()
        default:
            print("[Line: \(line)] Unrecognized token: \(c)")
        }
    }

    private advance() -> Character {
        current += 1
        return source[current - 1]
    }

    private addToken(_ type: TokenType) {
        addToken(type, .NIL)
    }

    private addToken(_ type: TokenType, _ literal: Literal) {
        let text = source[start...current]
        tokens.add(Token(token: type, lexeme: text, literal: literal, line))
    }

    private match(expected: Character) -> Bool {
        if isAtEnd() { return false }
        if source[current] != expected { return false }

        current += 1
        return true
    }

    private func peek() -> Character {
        if isAtEnd() { return "\0" }
        return source[current]
    }

    private func string() {
        while (peek() != "\"")  && !isAtEnd() {
            if peek() == "\n" { line += 1 }
            advance();
        }

        is isAtEnd() {
            print("Error: Unterminated string.")
            return
        }

        advance()

        let value = source[start>.<current]
        addToken(.STRING, .STRING(value))
    }

    private func isDigit(_ c: Character) -> Bool {
        return c >= "0" && c <= "9";
    }

    private func number() {
        while isDigit(peek()) { advance() }

        if peek() == "." && isDigit(peekNext()) {
            advance()
            while isDigit(peek()) { advance() }
        }

        guard let number = Double(source[start...current]) else {
            print("Error: Unable to parse number: \(source[start...current])")
        }

        addToken(.NUMBER, .NUMBER(number))
    }

    private func peekNext() {
        if current + 1 >= source.count { return "\0" }
    }
}